import { Fragment } from 'react';
import Banner from '../components/Banner';
import React from 'react';
import { Button } from 'react-bootstrap';
import Highlights from '../components/Highlights';
import ProductCard from '../components/Product';
import { Link } from 'react-router-dom';

export default function Home() {
  const data = {
    title: "Adobo Putoshop",
    content: "Alam ko gutom ka na.",
    destination: "/products",
  };

  return (
    <Fragment>
      <div className="text-center">
        <Banner data={data} />
        <Link className="btn btn-primary m-2" block="true" to="/products">
          Padeliver ka na!
        </Link>
        <Highlights />
      </div>
    </Fragment>
  );
}
